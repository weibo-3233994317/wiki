## 一、技术栈
SpringBoot 2.x  |  Vue3  |  Mybatis  |  Mysql  |  Redis  |  RocketMQ  |  WebSocket  |  Nginx
### 1、后端
```
1）基于SpringBoot 2.x，使用Mysql作为数据库，Mybatis作为数据持久层，并使用mybatis-generator辅助生成Domain实例、Mapper接口和对应Sql脚本文件
2）使用PageHelper实现后端分页（若不用该插件，则写两个sql，一个查询当前页列表，一个查询总笔数）
    Mysql分页查询
        select * from table_name limit 0,5  从第1条开始，查5条数据（初始偏移量为0）
    Informix分页查询
        select skip 0 first 5 * from table_name 从第1条开始，查5条数据（初始偏移量为0）
3）使用雪花算法，进行相关ID的生成
    用5位作为数据中心id，5位作为机器id，12位作为序列号，能支持1024个机器、单个机器生成不碰撞序列的TPS能达到4百多万（2^12=4096/ms，即409.6W/s）
4）使用Redis，存储登录后的凭证信息，并进行互剔（token--UserInfo，masterId--token）
5）使用WebSocket，实现后端向前端的消息通知（长连接，用户量较多的大型网站，不建议使用）
6）使用RocketMQ解耦点赞通知功能（点赞->生产MQ->消费MQ->推送WS消息->弹出通知）
7）使用AOP进行统一日志输出
8）使用Spring拦截器，进行登录校验等权限控制
9）定时任务，负责汇总电子书统计信息，以及每天的电子书快照（数据用于报表和趋势图）
```
### 2、前端
```
1）基于Vue3搭建，使用Ant Design Vue作为UI组件库
2）使用axios负责与后端的通讯
3）使用wangEditor作为富文本插件
4）使用Vuex保存用户信息，用于组件之间共享登录信息（并整合sessionStorage，解决刷新数据丢失问题）
5）使用echarts，实现趋势图
```
## 二、生产环境部署
### 1、RDS准备
```
1、购买RDS实例
2、新建账户wiki
3、新建数据库wiki
4、查看实例的内外网地址，并设置白名单（实例基本信息--查询连接详情，可查看）
5、后端应用，访问RDS数据库，测试联通情况
```

### 2、ECS准备
```
1、购买ECS实例
2、配置安全组（管理--安全组--配置规则）
    入方向，至少要提供22和80端口（22用于远程连接，80用于访问nginx）
3、配置jdk
    1）上传 jdk-8u311-linux-x64.tar.gz
    2）解压缩 tar -zxvf jdk-8u311-linux-x64.tar.gz  （ssh root@xx.xx.xx.xx，远程登录后操作）
    3）配置环境变量
      ① 修改/etc/profile，增加如下内容：
          export JAVA_HOME=/root/software/jdk1.8
          export PATH=$JAVA_HOME/bin:$PATH
          export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
      ② source /etc/profile （使环境变量生效）
    4）验证jdk配置情况（java -version）  
4、安装配置nginx
    1）在线安装 yum install nginx （ssh root@xx.xx.xx.xx，远程登录后操作）
    2）改用root用户启动nginx（/etc/nginx/nginx.conf，因为用root用户操作ECS服务器，使用默认nginx用户可能没有部分目录权限）
    3）启动nginx（service nginx start）
    4）访问验证
        curl http://xx.xx.xx.xx （缺省为80端口，nginx配置该端口监听）
        浏览器访问 http://xx.xx.xx.xx
        均能看到nginx返回的页面：Hello, Hunting Beagle!，说明nginx启动成功
```
### 3、后端应用准备
```
1、mvn package 或 mvn install，进行maven项目打包
2、将 zytwiki.jar、停启shell脚本 上传到ECS服务器（/root/wiki）
3、启动服务 sh start.sh （确保日志目录存在 /root/wiki/logs，否则启动报错）
4、可测试一下后端服务器的接口，但要在ECS实例安全组的临时开放一下8880端口
5、创建业务逻辑所需目录
    mkdir -p /businessdata/wiki/temp/ebookcover
    mkdir -p /businessdata/wiki/work/ebookcover
```
### 4、前端应用准备
```
1、npm run build-prod，对前端项目进行生产编译
2、将前端编译文件（dist文件夹下），全部上传到ECS服务器（/root/wiki/web/）
3、增加nginx静态网站配置
    新建web.conf上传到/etc/nginx/conf.d 中，里面配置监听80端口，访问定位到/root/wiki/web/中的index.html页面
4、重新加载nginx（nginx -s reload，可重复执行不要紧）
5、浏览器访问ECS外网地址，查看页面展示情况
```
