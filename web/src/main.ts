import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import * as Icons from '@ant-design/icons-vue';
import axios from 'axios';
import {Tool} from "@/util/tool";
import { message} from 'ant-design-vue';

axios.defaults.baseURL = process.env.VUE_APP_SERVER;

/* axios拦截器 */
axios.interceptors.request.use(function (config){
    console.log("请求参数：", config) ;
    // 请求headers增加token
    const token = store.state.user.token ;  // router/index.js中的写法，能确保user没有值时拿到空对象，避免user.token出现空指针异常
    if(Tool.isNotEmpty(token)){
        console.log("请求headers增加token:", token);
        config.headers.token = token ;
    }
    return config ;
}, error => {
    return Promise.reject(error);
})
axios.interceptors.response.use(function(response){
    console.log("返回结果：", response) ;
    return response ;
}, error => {
    console.log('返回错误：', error);
    const response = error.response ;
    const status = response.status ;
    if(status === 401){
        // 判断状态码是401，跳转到首页或登录页 【否则页面看不出效果，并且能一直看到登录后才可看到的元素（即使刷新页面），但又不能有效成功提交操作】
        console.log("未登录，跳转到首页") ;
        store.commit("setUser", {}) ;   // 将会话中的用户信息置空
        message.error("未登录或登录超时") ;
        router.push('/') ;  // 路由到首页
    }
    return Promise.reject(error);
})

const app = createApp(App);
app.use(store).use(router).use(Antd).mount('#app');

// 全局使用图标
const icons: any = Icons;
for (const i in icons) {
  app.component(i, icons[i]);
}
// 使用 process.env.xxx 读环境变量
console.log("环境：", process.env.NODE_ENV)
// 环境变量中自定义参数，必须以VUE_APP_开头
console.log("服务器：", process.env.VUE_APP_SERVER)
