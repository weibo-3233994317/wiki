import { createStore } from 'vuex'

declare let SessionStorage: any; // vuex封装sessionStorage，各页面/组件只需知道vuex，不需要知道sessionStorage
const USER = "USER" ;

const store =  createStore({
  // 在state中定义全局响应式变量
  state: {
    // 初始化时，直接从sessionStora中获取（页面刷新时会触发初始化）【小技巧】：避免空指针（若sessionStorage中拿不到，就是一个空对象，其他地方直接操作就会undefined）
    user: SessionStorage.get(USER) || {}
  },
  // 对全局变量的操作【同步】
  mutations: {
    // 参数1，是全局的state，通过它来操作里面的user ；参数2，外部的变量
    setUser (state, user) {
        state.user = user ;                 // 给vuex中的全局响应式变量赋值
        SessionStorage.set(USER, user) ;    // 给sessionStorage中赋值
    }
  },
  // 对全局变量的操作【支持异步】
  actions: {
  },
  modules: {
  }
});

export default store ; // 外部通过store这个名字来使用，默认的写法 exprot default createStore(...) 是没有名字的