#!/bin/bash
echo "--- begin publish ---"

process_id=`ps -ef | grep zytwiki.jar | grep -v grep | awk '{print $2}'`
if [ -n "$process_id" ] ; then
 sudo kill -9 $process_id
fi

# 环境变量生效
source /etc/profile
# 后台启动（&放最后表示程序后台执行，一般与nohup一起用） ； 标注输出到/dev/null（表示不写） ； 2>&1（标准错误重定向到标准输出，2--标准错误stderr，&1--标准输出stdout）
nohup java -Xms512m -Xmx512m -Dspring.profiles.active=prod -jar  ~/wiki/zytwiki.jar > /root/wiki/logs/wiki.out 2>&1 &

# 启动java应用，并输出gc日志
#nohup java -Xms512m -Xmx512m -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:/root/wiki/logs/wiki_gc.log -Dspring.profiles.active=prod -jar  ~/wiki/zytwiki.jar > /root/wiki/logs/wiki.out 2>&1 &

echo "--- end publish ---"