package com.zyt.wiki.mapper;

import org.apache.ibatis.annotations.Param;

public interface DocMapperCust {

    int increaseViewCount(@Param("id") Long id);   // 注意，这里的@Param注解要用ibatis包中的

    int increaseVoteCount(@Param("id") Long id);

    int updateEbookInfo() ;

}