package com.zyt.wiki.mapper;

import com.zyt.wiki.domain.Test;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

//@Mapper
public interface TestMapper {
    // public abstract 是默认修饰符
    List<Test> list() ;

}
