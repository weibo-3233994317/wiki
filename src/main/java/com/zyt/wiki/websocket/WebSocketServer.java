package com.zyt.wiki.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;

@Component
@ServerEndpoint("/ws/{token}")  //有点类似Controller注解，Controller是开放Http接口，这个是开放WebSocket连接
public class WebSocketServer {

    public static final Logger LOG = LoggerFactory.getLogger(WebSocketServer.class) ;

    /**
     * 每个客户端一个token（由客户端自己生成，保证唯一） （每个客户端创建WebSocket连接时，可以理解成这个Server会new出一个对象）
     */
    private String token = "" ;

    /**
     * 缓存所有WebSocket连接（类静态属性，只会有一份）
     */
    private static HashMap<String, Session> map = new HashMap<>() ;

    /** 连接成功 */
    @OnOpen
    public void onOpen(Session session, @PathParam("token") String token) {
        map.put(token, session);
        this.token = token;
        LOG.info("有新连接：token：{}，session id：{}，当前连接数：{}", token, session.getId(), map.size());
    }

    /** 连接关闭 */
    @OnClose
    public void onClose(Session session) {
        map.remove(this.token);
        LOG.info("连接关闭，token：{}，session id：{}！当前连接数：{}", this.token, session.getId(), map.size());
    }

    /** 收到消息 */
    @OnMessage
    public void onMessage(String message, Session session) {
        LOG.info("收到消息：token：{}，内容：{}", token, message);
    }

    /** 连接错误 */
    @OnError
    public void onError(Session session, Throwable error) {
        LOG.error("发生错误", error);
    }

    /** 群发消息 */
    public void sendInfo(String message) {
        for (String token : map.keySet()) {
            Session session = map.get(token);
            try {
                session.getBasicRemote().sendText(message);
            } catch (IOException e) {
                LOG.error("推送消息失败：token：{}，内容：{}", token, message);
            }
            LOG.info("推送消息：token：{}，内容：{}", token, message);
        }
    }

}
