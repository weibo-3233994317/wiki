package com.zyt.wiki.exception;

public class BusinessException extends RuntimeException{

    private BusinessExceptionCode code ;

    public BusinessException(BusinessExceptionCode code) {
        super(code.getDesc());
        this.code = code ;
    }

    public BusinessExceptionCode getCode() {
        return code;
    }

    public void setCode(BusinessExceptionCode code) {
        this.code = code;
    }

    /**
     * 不写入堆栈信息，提高性能（因为自定义异常基本都是很确切的，不需要整个堆栈）
     */
    @Override
    public Throwable fillInStackTrace() {
        return this ;
    }
}
