package com.zyt.wiki.exception;

public enum BusinessExceptionCode {

    USER_LOGIN_NAME_EXIST("1", "登录名已存在") ,
    LOGIN_USER_ERROR("2", "登录名不存在或密码错误") ,
    VOTE_REPEAT("3", "您已点赞过") ,
    SAVE_FILE_ERROR("4", "保存文件失败") ,
    MOVE_FILE_ERROR("5", "移动文件失败") ,
    ;

    private String code ;
    private String desc ;

    BusinessExceptionCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
