//package com.zyt.wiki.filter;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.*;
//import javax.servlet.http.HttpServletRequest;
//import java.io.IOException;
//
///**
// * 过滤器：是servlet的一个概念，而servlet又是web容器（tomcat、netty等）的一个概念，所以过滤器是给web容器用的
// */
//@Component
//public class LogFilter implements Filter {
//
//    public static final Logger LOG = LoggerFactory.getLogger(LogFilter.class) ;
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        // 打印请求信息
//        LOG.info("------------- LogFilter 开始 -------------");
//        HttpServletRequest request = (HttpServletRequest) servletRequest ;
//        LOG.info("请求地址: {} {}", request.getRequestURL(), request.getMethod());
//        LOG.info("远程地址: {}", request.getRemoteAddr());
//
//        long startTime = System.currentTimeMillis() ;
//        filterChain.doFilter(servletRequest, servletResponse);
//        LOG.info("------------- LogFilter 结束 耗时: {} ms -------------", System.currentTimeMillis() - startTime);
//    }
//}
