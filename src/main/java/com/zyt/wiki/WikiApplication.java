package com.zyt.wiki;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.zyt.wiki")  // 这里用@MapperScan，就可以不用在每个Mapper类上用@Mapper了
@EnableScheduling
@EnableAsync
public class WikiApplication{

    public static final Logger LOG = LoggerFactory.getLogger(WikiApplication.class) ;
    public static void main(String[] args) {
        SpringApplication.run(WikiApplication.class, args);
        LOG.info("启动成功！");
    }
}
