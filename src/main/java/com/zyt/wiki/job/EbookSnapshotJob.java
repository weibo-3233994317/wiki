package com.zyt.wiki.job;

import com.zyt.wiki.service.EbookSnapshotService;
import com.zyt.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class EbookSnapshotJob {

    public static final Logger LOG = LoggerFactory.getLogger(EbookSnapshotJob.class) ;

    @Resource
    private EbookSnapshotService ebookSnapshotService ;

    @Resource
    private SnowFlake snowFlake ;

    /**
     * 定时汇总电子书快照表信息（只有等上一次执行完成，下一次才会在下一个时间点执行，错过就错过）
     * （若只配置凌晨定时执行一次，遇到投产或其他停机情况，则会错过跑批，故设计为每天多次跑批。该方案需要sql编写时注意。）
     * （生产可以每半小时或1小时，测试可以配置的快一点）
     */
    @Scheduled(cron = "0 0/5 * * * ?")
    public void updateEbookInfoJob(){
        // 增加日志流水号 (也可以针对定时任务,加一个AOP,但本项目定时任务较少,就直接在这儿加了)
        MDC.put("LOG_ID", String.valueOf(snowFlake.nextId()));
        LOG.info("生成今日电子书快照 开始");
        long startTime = System.currentTimeMillis() ;
        this.ebookSnapshotService.genSnapshot();
        LOG.info("生成今日电子书快照 结束, 耗时: {} ms", System.currentTimeMillis() - startTime);
    }

}
