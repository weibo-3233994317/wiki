package com.zyt.wiki.job;

import com.zyt.wiki.service.DocService;
import com.zyt.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class DocJob {

    public static final Logger LOG = LoggerFactory.getLogger(DocJob.class) ;

    @Resource
    private DocService docService ;

    @Resource
    private SnowFlake snowFlake ;

    /**
     * 每30秒更新电子书信息 (从第5秒开始,每30秒执行一次)
     * (不要所有定时任务都设置成从0开始,建议避免所有定时器都在同一时间点触发)
     */
//    @Scheduled(cron = "5/30 * * * * ?")
    @Scheduled(cron = "0 0/1 * * * ?")
    public void updateEbookInfoJob(){
        // 增加日志流水号 (也可以针对定时任务,加一个AOP,但本项目定时任务较少,就之间在这儿加了)
        MDC.put("LOG_ID", String.valueOf(snowFlake.nextId()));

        LOG.info("电子书下的文档数据更新开始");
        long startTime = System.currentTimeMillis() ;
        this.docService.updateEbookInfo();
        LOG.info("电子书下的文档数据更新结束, 耗时: {} ms", System.currentTimeMillis() - startTime);
    }

}
