//package com.zyt.wiki.job;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
////import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//@Component
//public class TestJob {
//
//    public static final Logger LOG = LoggerFactory.getLogger(TestJob.class) ;
//
//    /** 固定时间间隔，fixedRate单位毫秒 */
////    @Scheduled(fixedRate = 5000)
//    public void simple() throws InterruptedException {
//        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss") ;
//        String dateStr = sdf.format(new Date());
//        Thread.sleep(7000);  // 休眠6秒
//        LOG.info("fixedRate 每隔5秒执行一次: {}", dateStr);
//    }
//
//    /**
//     * 自定义cron表达式跑批
//     * 注1:只有等上一次执行完成，下一次才会在下一个时间点执行，错过就错过
//     * 注2:且因为当前没有使用异步线程池, corn和上面的fixedRate若同时启动, 都使用Schedule默认线程, 同一时刻只能有一个任务在执行
//     */
////    @Scheduled(cron = "*/5 * * * * ?")
//    public void cron() throws InterruptedException {
//        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss") ;
//        String dateStr = sdf.format(new Date());
//        Thread.sleep(7000);  // 休眠6秒
//        LOG.info("cron 每隔5秒执行一次: {}", dateStr);
//    }
//
//}
