package com.zyt.wiki.interceptor.rate.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "rate.token.filter.ip-filter")
public class RateTokenIpProperties {

    private boolean enabled ;   //是否开启
    private long bucketLimit ;  // 桶最大容量
    private long tokenPerInterval;  // 单次放入令牌数量
    private long intervalInMills ;  // 间隔时间（单位：毫秒）

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public long getBucketLimit() {
        return bucketLimit;
    }

    public void setBucketLimit(long bucketLimit) {
        this.bucketLimit = bucketLimit;
    }

    public long getTokenPerInterval() {
        return tokenPerInterval;
    }

    public void setTokenPerInterval(long tokenPerInterval) {
        this.tokenPerInterval = tokenPerInterval;
    }

    public long getIntervalInMills() {
        return intervalInMills;
    }

    public void setIntervalInMills(long intervalInMills) {
        this.intervalInMills = intervalInMills;
    }

}
