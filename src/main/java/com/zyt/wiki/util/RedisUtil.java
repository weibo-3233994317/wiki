package com.zyt.wiki.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtil {

    public static final Logger LOG = LoggerFactory.getLogger(RedisUtil.class) ;

    @Resource
    private RedisTemplate redisTemplate ;

    /**
     * 判断一个key是否已存在（若不存在，则放入redis）【该方法存在并发小问题，同一个IP并发点赞同一个文档，会能多次点赞】
     * @param key   redis中的key
     * @param second    过期时间，单位：秒
     * @return  true--key不存在，放入一个key ； false--key已存在
     */
    public boolean validateRepeat(String key, long second){
        if(this.redisTemplate.hasKey(key)){
            LOG.info("redis中key已存在：{}", key);
            return false ;
        }else{
            LOG.info("redis中key不存在，放入：{}，有效期 {} 秒", key, second);
            this.redisTemplate.opsForValue().set(key,key, second, TimeUnit.SECONDS);
            return true ;
        }
    }

    /**
     * 判断一个key是否已存在（若不存在，则放入redis）【使用setNX，解决上面方法的并发小问题，因为redis接收请求是单线程，一个成功后，其他人都不会再成功】
     * @param key   redis中的key
     * @param second    过期时间，单位：秒
     * @return  true--key不存在，放入一个key ； false--key已存在
     */
    public boolean validateRepeat1(String key, long second){
        // 使用分布式锁setNX（key不存在则设置并返回true，否则不设置直接返回false）
        boolean success = this.redisTemplate.opsForValue().setIfAbsent(key,key, second, TimeUnit.SECONDS) ;
        if(!success){
            LOG.info("redis中key已存在：{}", key);
            return false ;
        }else{
            LOG.info("redis中key不存在，放入：{}，有效期 {} 秒", key, second);
            return true ;
        }
    }

}
