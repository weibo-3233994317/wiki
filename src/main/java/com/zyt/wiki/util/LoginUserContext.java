package com.zyt.wiki.util;

import com.zyt.wiki.resp.UserLoginResp;

public class LoginUserContext {

    // 线程本地存储。用于在整个线程链路上，进行数据存取。eg：前面（如AOP切面逻辑中）存，后面（如Service层）取
    private static ThreadLocal<UserLoginResp> user = new ThreadLocal<>() ;

    public static UserLoginResp getUser(){
        return user.get() ;
    }

    public static void setUser(UserLoginResp userLoginResp){
        LoginUserContext.user.set(userLoginResp) ;
    }

}
