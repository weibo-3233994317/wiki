package com.zyt.wiki.util;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class FileUtil {

    public static final Logger LOG = LoggerFactory.getLogger(FileUtil.class) ;

    /**
     * 将前端上传的文件存放到指定目录
     * @param file      前端上送文件对象MultipartFile
     * @param targetPath    目标文件目录
     * @param targetFileName    目标文件名
     * @return  true：保存成功   false：保存失败
     */
    public static boolean saveFile(MultipartFile file, String targetPath, String targetFileName) {
        File targetFilePath = new File(targetPath) ;
        // 目标目录不存在，则创建
        if(!targetFilePath.exists()){
            targetFilePath.mkdirs() ;
        }
        File targetFile = new File(targetFilePath, targetFileName) ;
        try {
            file.transferTo(targetFile);
        }catch (IOException e){
            LOG.error("文件保存失败，路径：{}，文件名：{}，异常信息：{}", targetFilePath, targetFile, e.getMessage());
            return false ;
        }
        return true ;
    }

    /**
     * 移动文件到指定目录
     * @param sourceFile    源文件
     * @param destFileDir    目标目录
     * @return  true：移动成功   false：移动失败
     */
    public static boolean moveFileToDir(String sourceFile, String destFileDir){
        File srcFile = new File(sourceFile) ;
        File desFileDir = new File(destFileDir) ;
        try {
            FileUtils.moveFileToDirectory(srcFile, desFileDir, true);
        } catch (IOException e) {
            LOG.error("移动文件失败！源文件：{}，目标目录：{}", srcFile, desFileDir);
            return false ;
        }
        return true ;
    }

}
