package com.zyt.wiki.util;

public class RequestContext {

    // 线程本地存储。用于在整个线程链路上，进行数据存取。eg：前面（如AOP切面逻辑中）存，后面（如Service层）取
    private static ThreadLocal<String> remoteAddr = new ThreadLocal<>() ;
//    private static ThreadLocal<String> userId = new ThreadLocal<>() ;

    public static String getRemoteAddr(){
        return remoteAddr.get() ;
    }

    public static void setRemoteAddr(String remoteAddr){
        RequestContext.remoteAddr.set(remoteAddr) ;
    }

}
