//package com.zyt.wiki.rocketmq;
//
//import com.zyt.wiki.websocket.WebSocketServer;
//import org.apache.rocketmq.common.message.MessageExt;
//import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
//import org.apache.rocketmq.spring.core.RocketMQListener;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//
///**
// * RocketMQ消费者  【一般要与生产者分别放在在不同的应用中，以实现业务功能解耦】
// */
//@Service
//@RocketMQMessageListener(consumerGroup = "default", topic = "VOTE_TOPIC")
//public class VoteTopicConsumer implements RocketMQListener<MessageExt> {
//
//    public static final Logger LOG = LoggerFactory.getLogger(VoteTopicConsumer.class) ;
//
//    @Resource
//    private WebSocketServer webSocketServer ;
//
//    @Override
//    public void onMessage(MessageExt messageExt) {
//        byte[] body = messageExt.getBody();
//        LOG.info("RocketMQ收到消息：{}", new String(body));
//        this.webSocketServer.sendInfo(new String(body));
//    }
//
//}
