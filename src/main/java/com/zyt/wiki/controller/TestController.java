package com.zyt.wiki.controller;

import com.zyt.wiki.domain.Test;
import com.zyt.wiki.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class TestController {

    public static final Logger LOG = LoggerFactory.getLogger(TestController.class) ;

    @Resource
    private RedisTemplate redisTemplate ;

//    @Value("${test.hello}")
    @Value("${test.hello:TEST}")    //冒号后面可以写上默认值，避免漏配时启动报错
    public String testHello ;

    @Autowired
    private TestService testService ;

    @RequestMapping("/hello")
    public String sayHello(){
        LOG.info("111111111111111111111111111111111");
        LOG.error("error###############");
        return "Hello World~ " + this.testHello;
    }

    @RequestMapping("/list")
    public List<Test> list(){
        return this.testService.list() ;
    }

    @RequestMapping("/redis/set/{key}/{value}")
    public String set(@PathVariable String key, @PathVariable String value) {
        this.redisTemplate.opsForValue().set(key, value, 3600, TimeUnit.SECONDS);
        LOG.info("key: {}, value: {}", key, value);
        return "success" ;
    }

    @RequestMapping("/redis/get/{key}")
    public Object get(@PathVariable String key) {
        Object obj = this.redisTemplate.opsForValue().get(key);
        LOG.info("key: {}, value: {}", key, obj);
        return obj ;
    }

    @RequestMapping("/redis/delete/{key}")
    public String delete(@PathVariable String key) {
        Object obj = this.redisTemplate.delete(key) ;
        return "success" ;
    }
}
