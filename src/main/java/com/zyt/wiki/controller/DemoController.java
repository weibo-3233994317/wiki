package com.zyt.wiki.controller;

import com.zyt.wiki.domain.Demo;
import com.zyt.wiki.service.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/demo")
public class DemoController {

    public static final Logger LOG = LoggerFactory.getLogger(DemoController.class) ;

    @Autowired
    private DemoService demoService ;

    @RequestMapping("/list")
    public List<Demo> list(){
        return this.demoService.list() ;
    }
}
