package com.zyt.wiki.controller;

import com.zyt.wiki.req.CategorySaveReq;
import com.zyt.wiki.resp.CategoryQueryResp;
import com.zyt.wiki.resp.CommonResp;
import com.zyt.wiki.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    public static final Logger LOG = LoggerFactory.getLogger(CategoryController.class) ;

    @Autowired
    private CategoryService categoryService ;

    // Get：http://localhost:8880/category/all
    @GetMapping("/all")
    public CommonResp all(){
        CommonResp<List<CategoryQueryResp>> resp = new CommonResp<>() ;
        List<CategoryQueryResp> list = this.categoryService.all();
        resp.setContent(list);
        return resp ;
    }

    // 【POST提交时】注意：
    //      前端若是application/x-www-form-urlencoded表单提交，则参数前面不用@RequestBody也能正常映射到参数；
    //      若是application/json方式提交，则需要@RequestBody把参数接收进来
    // axios的post提交，是用application/json方式提交的，所以要加@RequestBody
    @PostMapping("/save")
    public CommonResp save(@Valid @RequestBody CategorySaveReq req){
        CommonResp resp = new CommonResp() ;
        this.categoryService.save(req) ;
        return  resp ;
    }

    @DeleteMapping("/delete/{id}")
    public CommonResp delete(@PathVariable Long id){
        CommonResp resp = new CommonResp() ;
        this.categoryService.delete(id) ;
        return resp ;
    }

}
