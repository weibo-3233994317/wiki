package com.zyt.wiki.controller;

import com.zyt.wiki.resp.CommonResp;
import com.zyt.wiki.resp.StatisticResp;
import com.zyt.wiki.service.EbookSnapshotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ebook-snapshot")
public class EbookSnapshotController {

    public static final Logger LOG = LoggerFactory.getLogger(EbookSnapshotController.class) ;

    @Autowired
    private EbookSnapshotService ebookSnapshotService ;

    /**
     * 获取首页数值数据（昨天+今天）：总阅读数、总点赞数、今日阅读数、今日点赞数、今日预计阅读数、今日预计阅读增长（后面两个字段靠前端自己计算）
     */
    @GetMapping("/get-statistic")
    public CommonResp getStatistic(){
        List<StatisticResp> statisticList = this.ebookSnapshotService.getStatistic();
        CommonResp<List<StatisticResp>> resp = new CommonResp<>() ;
        resp.setContent(statisticList);
        return resp ;
    }

    /**
     * 30天数值统计（日期、每天的阅读数、每天的点赞数）
     */
    @GetMapping("/get-30-statistic")
    public CommonResp get30Statistic(){
        List<StatisticResp> statisticList = this.ebookSnapshotService.get30Statistic();
        CommonResp<List<StatisticResp>> resp = new CommonResp<>() ;
        resp.setContent(statisticList);
        return resp ;
    }

}
