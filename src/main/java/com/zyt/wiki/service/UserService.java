package com.zyt.wiki.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zyt.wiki.domain.User;
import com.zyt.wiki.domain.UserExample;
import com.zyt.wiki.exception.BusinessException;
import com.zyt.wiki.exception.BusinessExceptionCode;
import com.zyt.wiki.mapper.UserMapper;
import com.zyt.wiki.req.UserLoginReq;
import com.zyt.wiki.req.UserQueryReq;
import com.zyt.wiki.req.UserResetPasswordReq;
import com.zyt.wiki.req.UserSaveReq;
import com.zyt.wiki.resp.PageResp;
import com.zyt.wiki.resp.UserLoginResp;
import com.zyt.wiki.resp.UserQueryResp;
import com.zyt.wiki.util.CopyUtil;
import com.zyt.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService {

    public static final Logger LOG = LoggerFactory.getLogger(UserService.class) ;

    @Autowired  // 【Spring自带】，通过 byType 的方式去注入的， 使用该注解，要求接口只能有一个实现类
    private UserMapper userMapper ;

    @Resource   // 【JDK自带】，通过 byName 和 byType的方式注入， 默认先按 byName的方式进行匹配，如果匹配不到，再按 byType的方式进行匹配
    private SnowFlake snowFlake ;

    /** 查询用户列表，分页 */
    public PageResp<UserQueryResp> list(UserQueryReq req){
        UserExample userExample = new UserExample() ;
        UserExample.Criteria criteria = userExample.createCriteria() ;
        // 若上送了登录名，则据此进行模糊查询
        if(!ObjectUtils.isEmpty(req.getLoginName())){
            criteria.andLoginNameLike("%" + req.getLoginName() + "%") ;
        }
        LOG.info("++++++++++++++++++++ pageNum: {}", req.getPageNum());
        LOG.info("++++++++++++++++++++ pageSize: {}", req.getPageSize());
        // 注意：1、仅对后面的第一个数据库操作有效 2、pageNum，从1开始 3、若pageSize为0，则不分页；若pageNum为0，则效果同第1页
        PageHelper.startPage(req.getPageNum(), req.getPageSize()) ;
        List<User> userList = this.userMapper.selectByExample(userExample);

        PageInfo pageInfo = new PageInfo(userList) ;
        LOG.info("总行数: {}", pageInfo.getTotal());
        LOG.info("总页数: {}", pageInfo.getPages());

        // 列表复制
        List<UserQueryResp> respList = CopyUtil.copyList(userList, UserQueryResp.class) ;

        PageResp<UserQueryResp> pageResp = new PageResp<>() ;
        pageResp.setTotal(pageInfo.getTotal());
        pageResp.setList(respList);
        return pageResp ;
    }

    /** 保存 */
    public void save(UserSaveReq req){
        User user = CopyUtil.copy(req, User.class) ;  // Mapper操作的是User对象，所以要转换一下
        if(ObjectUtils.isEmpty(user.getId())){
            // 前端没有上送主键Id，说明时新增操作（新增用户时，登录名不能重复）
            User userDB = this.selectByLoginName(req.getLoginName());
            if(!ObjectUtils.isEmpty(userDB)){   // 登录名已存在
                throw new BusinessException(BusinessExceptionCode.USER_LOGIN_NAME_EXIST) ;
            }
            user.setId(this.snowFlake.nextId());
            this.userMapper.insert(user) ;
        }else {
            // 更新（登录名、密码 不允许修改）
            user.setLoginName(null);
            user.setPassword(null);
            this.userMapper.updateByPrimaryKeySelective(user) ; // 这个Selective的方法，针对user里面的属性有值才更新，没有值的字段不更新
        }
    }

    /** 删除 */
    public void delete(Long id){
        this.userMapper.deleteByPrimaryKey(id) ;
    }

    /** 根据登录名，查询用户表 */
    public User selectByLoginName(String loginName){
        UserExample userExample = new UserExample() ;
        userExample.createCriteria().andLoginNameEqualTo(loginName) ;
        List<User> userList = this.userMapper.selectByExample(userExample);
        if(ObjectUtils.isEmpty(userList)) {
            return null ;
        }else {
            return userList.get(0) ;
        }
    }

    /** 修改密码 */
    public void resetPassword(UserResetPasswordReq req){
        User user = CopyUtil.copy(req, User.class) ;  // req中只有id和password，所以下面只会更新密码
        this.userMapper.updateByPrimaryKeySelective(user) ;
    }

    /** 登录 */
    public UserLoginResp login(UserLoginReq req){
        // 根据登录名，查询数据库
        User userDb = this.selectByLoginName(req.getLoginName()) ;
        if(ObjectUtils.isEmpty(userDb)){
            // 用户名不存在
            LOG.info("登录名不存在, {}", req.getLoginName());
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR) ;
        }
        if(!userDb.getPassword().equals(req.getPassword())){
            // 密码不对
            LOG.info("密码不对，输入密码: {}, 数据库密码: {}", req.getPassword(), userDb.getPassword());
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR) ;
        }
        UserLoginResp userLoginResp = CopyUtil.copy(userDb, UserLoginResp.class);
        return  userLoginResp ;
    }
}
