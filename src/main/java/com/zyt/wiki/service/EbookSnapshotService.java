package com.zyt.wiki.service;

import com.zyt.wiki.mapper.EbookSnapshotMapperCust;
import com.zyt.wiki.resp.StatisticResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class EbookSnapshotService {

    public static final Logger LOG = LoggerFactory.getLogger(EbookSnapshotService.class) ;

    @Resource
    private EbookSnapshotMapperCust ebookSnapshotMapperCust ;

    /** 生成今日电子书快照 */
    public void genSnapshot(){
        this.ebookSnapshotMapperCust.genSnapshot() ;
    }

    /**
     * 获取首页数值数据（昨天+今天）：总阅读数、总点赞数、今日阅读数、今日点赞数、今日预计阅读数、今日预计阅读增长（后面两个字段靠前端自己计算）
     */
    public List<StatisticResp> getStatistic(){
        return this.ebookSnapshotMapperCust.getStatistic() ;
    }

    /**
     * 30天数值统计（日期、每天的阅读数、每天的点赞数）
     */
    public List<StatisticResp> get30Statistic(){
        return this.ebookSnapshotMapperCust.get30Statistic() ;
    }

}
