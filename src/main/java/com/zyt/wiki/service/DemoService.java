package com.zyt.wiki.service;

import com.zyt.wiki.domain.Demo;
import com.zyt.wiki.mapper.DemoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DemoService {

    @Autowired
    private DemoMapper demoMapper ;

    public List<Demo> list(){
        return this.demoMapper.selectByExample(null) ;
    }
}
