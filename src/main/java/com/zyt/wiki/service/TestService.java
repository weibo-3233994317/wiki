package com.zyt.wiki.service;

import com.zyt.wiki.domain.Test;
import com.zyt.wiki.mapper.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestService {

    @Autowired
    private TestMapper testMapper ;

    public List<Test> list(){
        return this.testMapper.list() ;
    }
}
