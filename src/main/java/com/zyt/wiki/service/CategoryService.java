package com.zyt.wiki.service;

import com.zyt.wiki.domain.Category;
import com.zyt.wiki.domain.CategoryExample;
import com.zyt.wiki.mapper.CategoryMapper;
import com.zyt.wiki.req.CategorySaveReq;
import com.zyt.wiki.resp.CategoryQueryResp;
import com.zyt.wiki.util.CopyUtil;
import com.zyt.wiki.util.SnowFlake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryService {

    public static final Logger LOG = LoggerFactory.getLogger(CategoryService.class) ;

    @Autowired  // 【Spring自带】，通过 byType 的方式去注入的， 使用该注解，要求接口只能有一个实现类
    private CategoryMapper categoryMapper ;

    @Resource   // 【JDK自带】，通过 byName 和 byType的方式注入， 默认先按 byName的方式进行匹配，如果匹配不到，再按 byType的方式进行匹配
    private SnowFlake snowFlake ;

    /** 查询分类列表，不分页 */
    public List<CategoryQueryResp> all(){
        CategoryExample categoryExample = new CategoryExample() ;
        categoryExample.setOrderByClause("sort asc");   //按sort字段升序
        List<Category> categoryList = this.categoryMapper.selectByExample(categoryExample);
        List<CategoryQueryResp> respList = CopyUtil.copyList(categoryList, CategoryQueryResp.class);
        return respList ;
    }

    /** 保存 */
    public void save(CategorySaveReq req){
        Category category = CopyUtil.copy(req, Category.class) ;  // Mapper操作的是Category对象，所以要转换一下
        if(ObjectUtils.isEmpty(category.getId())){
            // 新增 （前端没有上送主键Id）
            category.setId(this.snowFlake.nextId());
            this.categoryMapper.insert(category) ;
        }else {
            // 修改
            this.categoryMapper.updateByPrimaryKey(category) ;
        }
    }

    /** 删除 */
    public void delete(Long id){
        this.categoryMapper.deleteByPrimaryKey(id) ;
    }

}
