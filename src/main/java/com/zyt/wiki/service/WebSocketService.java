package com.zyt.wiki.service;

import com.zyt.wiki.websocket.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class WebSocketService {

    public static final Logger LOG = LoggerFactory.getLogger(WebSocketService.class) ;

    @Resource
    private WebSocketServer webSocketServer ;

    @Async  // 该注解不能直接加到sendInfo上，会与@ServerEndpoint冲突，启动报错
    public void sendInfoAsync(String messsage, String logId){
        MDC.put("LOG_ID", logId); // 设置原线程的日志流水号，便于业务流程日志跟踪
        LOG.info("异步化消息发送方法被调用");
        this.webSocketServer.sendInfo(messsage);
    }

}
