package com.zyt.wiki.config;

import com.zyt.wiki.interceptor.ActionInterceptor;
import com.zyt.wiki.interceptor.IllegalRequestInterceptor;
import com.zyt.wiki.interceptor.LoginInterceptor;
import com.zyt.wiki.interceptor.rate.RateTokenByIpInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class SpringMvcConfig implements WebMvcConfigurer {

    @Resource
    RateTokenByIpInterceptor rateTokenByIpInterceptor ;
    @Resource
    IllegalRequestInterceptor illegalRequestInterceptor ;
    @Resource
    LoginInterceptor loginInterceptor ;
    @Resource
    ActionInterceptor actionInterceptor ;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 将自定义拦截器添加到SpringMvc中，拦截除了指定pattern以外的所有请求【权限校验时，用这种方式配置拦截的地址，比过滤器和AOP更简洁】
        // 拦截器先后顺序，默认以addInterceptor的顺序，也可以通过.order(n)方法设置，值越小，拦截器的优先级越高
        registry.addInterceptor(illegalRequestInterceptor)
                .addPathPatterns("/**") ;
        registry.addInterceptor(rateTokenByIpInterceptor)
                .addPathPatterns("/**");
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(
                        "/test/**",
                        "/redis/**",
                        "/user/login",
                        "/ebook/all",
                        "/ebook/list",
                        "/category/all",
                        "/doc/all/**",
                        "/doc/find-content/**",
                        "/doc/vote/**",
                        "/ebook-snapshot/**",
                        "/ebook/load-image/**"
                );

        registry.addInterceptor(actionInterceptor)
                .addPathPatterns(
                        "/*/save",
                        "/*/delete/**",
                        "/*/reset-password",
                        "/*/upload-image"
                );

    }
}
